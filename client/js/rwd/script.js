﻿jQuery(document).ready(function ($) {
    /*======modal Add cart======*/
    $('#modalAddCart').on('show.bs.modal', function (event) {
        $(this).next(".modal-backdrop").removeClass("hide");
        $('body').addClass("modal-visible");

    });
    $('#modalAddCart').on('hidden.bs.modal', function () {
        $(this).next(".modal-backdrop").addClass("hide");
        $('body').removeClass("modal-visible");
    });

    /*=====handle event search navbar======*/
    $('body').on('click', function () {
        $(".dropdown-menu").removeClass("open");
    });


    carouselChangeArrow($('#slide-product-generic'));
    $('#slide-product-generic').bind('slid.bs.carousel', function (e) {
        carouselChangeArrow($('#slide-product-generic'));
    });
    carouselChangeArrow($('#slide-logos-generic'));
    $('#slide-logos-generic').bind('slid.bs.carousel', function (e) {
        carouselChangeArrow($('#slide-logos-generic'));
    });
    hideArrowsCarousel($('#slide-logos-generic'));

    /*plugin button plus-minus*/
    $('.btn-number').click(function (e) {
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        type = $(this).attr('data-type');
        var input = $("input[name='" + fieldName + "']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {

                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if (parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if (type == 'plus') {

                if (currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if (parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });


    $('.input-number').focusin(function () {
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function () {

        minValue = parseInt($(this).attr('min'));
        maxValue = parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if (valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if (valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });


    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

});

/*=====event search navbar=====*/
function openDropdownSearch(current) {
    event.stopPropagation();
    $(current).next(".dropdown-menu").addClass("open");

}

//check if to change the arrow on carousel element
function carouselChangeArrow(element) {
    var firstItem = element.find('.carousel-inner .item:first');
    var lastItem = element.find('.carousel-inner .item:last');
    var arrowRight = element.find('.slide-control .left');
    var arrowLeft = element.find('.slide-control .right');

    if (firstItem.hasClass('active')) {
        arrowRight.addClass("disabled");
    } else {
        arrowRight.removeClass("disabled");
    }
    if (lastItem.hasClass('active')) {
        arrowLeft.addClass("disabled");
    } else {
        arrowLeft.removeClass("disabled");
    }
}

function hideArrowsCarousel(element) {
    var items = element.find('.carousel-inner .item');
    if (items.length == 1) {
        element.addClass("oneitem");
    }
}